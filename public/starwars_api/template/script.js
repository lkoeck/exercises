function createCard(name, birthyear, homeworldurl){
    var div1 = document.createElement("div");
    var div2 = document.createElement("div");
    var div3 = document.createElement("div");
    var h41 = document.createElement("h4");
    var h42 = document.createElement("h4");
    var button = document.createElement("button");
    var h3 = document.createElement("h3");

    div1.className = "col-sm";
    div2.className = "card";
    div3.className = "card-body";
    h41.className = "card-title";
    h41.innerHTML = name;
    h42.className = "card-subtitle";
    h42.innerHTML = birthyear;
    h42.className += "mb-2";
    h42.className += "text-muted";
    h3.className = "card-text";
    h3.style.display = "none";
    button.className = "btn";
    button.className += "btn-dark";
    button.innerHTML = "Heimat anzeigen";
    button.addEventListener("click", function(){
        button.style.display = "none";
        loadHomeWorld(homeworldurl, h3);
        h3.style.display = "initial";
    });

    div3.appendChild(h41);
    div3.appendChild(h42);
    div3.appendChild(button);
    div3.appendChild(h3);
    div2.appendChild(div3);
    div1.appendChild(div2);

    document.getElementById("people").appendChild(div1);
}

var url = 'https://swapi.co/api/people/';
function onload() {
    fetch(url, {method: 'GET'}).then(response => {
        if (response.status !== 200) {
            console.log('Es ist ein Fehler aufgetreten' + response.status);
            return;
        }
        response.json().then(
            function (data) {
                let results = data.results;
                for (var i of results) {
                    createCard(i.name, i.birth_year, i.homeworld);
                }
            });
    });
}

function loadHomeWorld(homeworld, element) {
    fetch(homeworld, {method: "GET"}).then(response =>{
        if (response.status !== 200) {
            console.log('Es ist ein Fehler aufgetreten' + response.status);
            return;
        }
        response.json().then( function (data) {
            element.innerHTML = "Heimat" + data.name
        })
    });
}
