function checklength() {
    var username = document.getElementById("username").value;
    if (username.length >= 3){
        document.getElementById("button").disabled = false;
    }
    else{
        document.getElementById("button").disabled = true;
    }
}

function link(){
    location.href = "list.html";
}

function chatStarten(){
    location.href = "chat.html";
}

function appendText(){
    var msg = document.getElementById("msg").value;
    var body = document.getElementById("body");
    
    var newtr = document.createElement("tr");
    var newtd = document.createElement("td");
    var tdtext = document.createTextNode(msg);
    newtd.appendChild(tdtext);
    newtr.appendChild(newtd);
    body.appendChild(newtr);
}