function createCard(title, subtitle) {
    let people = document.getElementById("people");

    let div1 = document.createElement("div");
    div1.classList.add('col-sm');
    let div2 = document.createElement("div");
    div2.classList.add('card');
    let div3 = document.createElement("div");
    div3.classList.add('card-body');
    div3.id = "div3";
    let cardtitle = document.createElement("h4");
    cardtitle.classList.add('card-title');
    cardtitle.innerHTML = title;
    let cardsubtitle = document.createElement("h4");
    cardsubtitle.classList.add('card-subtitle','mb-2','text-muted');
    cardsubtitle.innerHTML = subtitle;
    let home = document.createElement("h3");
    home.style.display = "none";
    let btn = document.createElement("button");
    btn.classList.add('btn','btn-dark');
    btn.innerHTML = 'Heimat anzeigen';
    btn.onchlick = function(){
        btn.style.display = "none";
        homeworld(title,home);
    }

    div3.appendChild(cardtitle);
    div3.appendChild(cardsubtitle);
    div3.appendChild(btn);
    div2.appendChild(div3);
    div1.appendChild(div2);

    people.appendChild(div1);
    //alert("i am here");
}

let url = 'https://swapi.co/api/people/';
const response = fetch(url);

function onload() {
    fetch(url, {method: 'GET'}).then(response => {
        if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' + response.status);
            return;
        }

        response.json().then(function (data) {
            let results = data.results;
            for (let i of results) {
                var name = i.name;
                var birthyear = i.birth_year;
                //alert(birthyear);
                createCard(name, birthyear);
            }
        });
    });
}

function homeworld(title,h3){
    fetch(url, {method: 'GET'}).then(response => {
        if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' + response.status);
            return;
        }

        response.json().then(function (data) {
            let results = data.results;
            //alert("HAllo")
            for (let i of results) {
                if(i.name == title){
                    var home = i.homeworld;
                    alert(home);
                    h3.innerHTML = i.homeworld;
                    h3.style.display ="inline";
                }
            }
        });
    });
}

